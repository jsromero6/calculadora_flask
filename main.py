from flask import Flask, render_template, request

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/')
def welcome():
    return render_template('calculadora.html')


@app.route('/result', methods=['POST'])
def result():
    num_1 = request.form.get("num_1", type=int)
    num_2 = request.form.get("num_2", type=int)
    operation = request.form.get("operacion")
    if(operation == 'Suma'):
        result = num_1 + num_2
    elif(operation == 'Resta'):
        result = num_1 - num_2
    elif(operation == 'Multiplicacion'):
        result = num_1 * num_2
    elif(operation == 'Division'):
        result = num_1 / num_2
    else:
        result = 'INVALID CHOICE'
    entry = result
    return render_template('resultado.html', entry=entry)

if __name__ == '__main__':
    app.run(debug=True)
